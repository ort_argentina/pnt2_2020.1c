// import Principal from '../components/Principal'

const routes = [
  {
    path: '/',
    component: () => import('../components/Principal'), // component: Principal
    children: [
      {
        path: 'hijo1',
        component: () => import('../components/PrincipalHijo1')
      },
      {
        path: 'hijo2',
        component: () => import('../components/PrincipalHijo2')
      }
    ]
  },
  {
    path: '/uno',
    component: () => import('../components/Uno'),
    children: [
      {
        path: 'el1',
        component: () => import('../components/UnoHijo1')
      }
    ]
  },
  {
    path: '/Dos',
    component: () => import('../components/Dos')
  }
]

export default routes
