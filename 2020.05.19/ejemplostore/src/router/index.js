import Vue from 'vue'
import Router from 'vue-router'
import Carrito from '@/pages/Carrito'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'carrito',
      component: Carrito
    },
    {
      path: '/checkout',
      name: 'checkout',
      component: () => import('@/pages/Checkout')
    }
  ]
})
