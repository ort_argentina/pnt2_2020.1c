import { AGREGAR_PRODUCTO, LOGIN } from '@/store/types'

export default {
  [LOGIN]: function (state, data) {
    state.username = data.usuario
    state.rol = data.rol
  },
  [AGREGAR_PRODUCTO]: function (state, item) {
    if (item.nombre !== '' && item.cantidad > 0) {
      state.cart.push({
        productName: item.nombre,
        quantity: item.cantidad
      })
    }
  },
  incrementar: function (state) {
    state.contador++
  }
}
