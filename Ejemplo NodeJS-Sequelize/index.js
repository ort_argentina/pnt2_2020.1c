const express = require('express');
const bodyParser = require('body-parser');

const routerDepartamentos = require('./routes/departamentos');
const routerClientes = require('./routes/clientes');

const db = require('./models');

const app = express();

app.use(bodyParser.json());
app.use('/departamento', routerDepartamentos);
app.use('/cliente', routerClientes);

app.listen(port=5000, () => {
    console.log('Servidor escuchando...');
});