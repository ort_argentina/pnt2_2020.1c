const Sequelize = require('sequelize');
const Cliente = require('./Cliente');
const Departamento = require('./Departamento');

let db = {};

// const sequelize = new Sequelize('mainDb', null, null, {
//     dialect: "sqlite",
//     storage: "./mainDB.sqlite"
// });

const sequelize = new Sequelize('', 'ort', '1234', {
    dialect: 'mssql',
    dialectModulePath: 'tedious',
    dialectOptions: {
        driver: 'SQL Server Native Client 11.0',
        instanceName: 'SQLSERVER2017',
        trustedConnection: true,
        encrypt: true
    },
    host: 'localhost',
    database: 'ort'
});

sequelize.authenticate()
    .then(function () { console.log('Autenticado'); })
    .catch(function (err) { console.log('Error autenticando: ' + err); })

db.Cliente = Cliente(sequelize, Sequelize);
db.Departamento = Departamento(sequelize, Sequelize);

db.Cliente.belongsTo(db.Departamento);
db.Departamento.belongsTo(db.Cliente);

sequelize.sync({ force: true })
    .then(function (err) {
        console.log('Modelo sincronizado.');
    });

module.exports = db;