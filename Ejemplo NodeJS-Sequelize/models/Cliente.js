module.exports = (sequelize, Sequelize) => {

    let Cliente = sequelize.define('Cliente', {
        nombre: Sequelize.STRING,
        apellido: Sequelize.STRING,
        edad: Sequelize.INTEGER
    });

    return Cliente;
};