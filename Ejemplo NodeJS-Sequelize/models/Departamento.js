module.exports = (sequelize, Sequelize) => {

    let Departamento = sequelize.define('Departamento', {
        descripcion: Sequelize.STRING
    });

    return Departamento;
};