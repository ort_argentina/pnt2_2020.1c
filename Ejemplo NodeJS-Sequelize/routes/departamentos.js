const express = require('express');
const router = express.Router();
const models = require('../models');

router.get('/', (req, res) => {
    models.Departamento.findAll()
        .then((rows) => {
            res.send(rows);
        });
});

router.get('/:id', (req, res) => {
    let id = req.params.id;

    models.Departamento.findOne({
        where: {
            id: id
        }
    })
        .then((row) => {
            res.send(row);
        });
});

router.post('/', (req, res) => {
    let descripcion = req.body.descripcion;

    models.Departamento.create({
        descripcion: descripcion
    }).then(() => {
        res.sendStatus(201);
    })
});

router.put('/:id', (req, res) => {
    let id = req.params.id;
    let descripcion = req.body.descripcion;

    models.Departamento.update(
        {
            descripcion: descripcion
        },
        {
            where: { id: id }
        }
    ).then(() => {
        res.sendStatus(200);
    })
});

router.delete('/:id', (req, res) => {
    let id = req.params.id;

    models.Departamento.destroy({
        where: {
            id: id
        }
    }).then(() => {
        res.sendStatus(200);
    })
});

module.exports = router;