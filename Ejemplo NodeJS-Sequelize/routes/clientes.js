const express = require('express');
const router = express.Router();
const models = require('../models');

router.get('/', (req, res) => {
    models.Cliente.findAll()
        .then((rows) => {
            res.send(rows);
        });
});

router.get('/:id', (req, res) => {
    let id = req.params.id;

    models.Cliente.findOne({
        where: {
            id: id
        }
    })
        .then((row) => {
            res.send(row);
        });
});

router.post('/', (req, res) => {
    let nombre = req.body.nombre;
    let apellido = req.body.apellido;
    let edad = req.body.edad;
    let departamentoid = req.body.departamentoid;

    models.Departamento.findOne({
        where: {
            id: departamentoid
        }
    })
        .then((departamento) => {
            models.Cliente.create({
                nombre: nombre,
                apellido: apellido,
                edad: edad
            }).then((cliente) => {

                cliente.setDepartamento(departamento);

                res.sendStatus(201);
            })
        })
});

router.put('/:id', (req, res) => {
    let id = req.params.id;
    let nombre = req.body.nombre;
    let apellido = req.body.apellido;
    let edad = req.body.edad;

    models.Cliente.update(
        {
            nombre: nombre,
            apellido: apellido,
            edad: edad
        },
        {
            where: { id: id }
        }
    ).then(() => {
        res.sendStatus(200);
    })
});

router.delete('/:id', (req, res) => {
    let id = req.params.id;

    models.Cliente.destroy({
        where: {
            id: id
        }
    }).then(() => {
        res.sendStatus(200);
    })
});

module.exports = router;